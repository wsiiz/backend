import { Controller, Get, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
@ApiBearerAuth()
export class UsersController {
  constructor(private service: UsersService) {}

  @Get('all')
  @ApiCreatedResponse({
    description: 'You can get all users.',
  })
  public async getAll(): Promise<void> {
    return await this.service.getAll();
  }

  @Get(':uuid')
  @ApiCreatedResponse({
    description: 'You can get user for uuid.',
  })
  public async getUser(@Param('id') id: number) {
    return await this.service.getUser(id);
  }

  @Post()
  @ApiCreatedResponse({
    description: 'You can get user for uuid.',
  })
  public async createUser() {
    return await this.service.createUser();
  }
}
