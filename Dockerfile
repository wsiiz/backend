# FROM node:14
# WORKDIR /usr/src/app
# COPY package*.json ./
# RUN npm install
# COPY . .
# EXPOSE 3000
# CMD ["npm", "run", "start:prod"]

# Określ obraz bazowy
FROM node:14

# Ustaw katalog roboczy w kontenerze
WORKDIR /usr/src/app

# Skopiuj pliki package.json oraz package-lock.json (jeśli istnieje)
COPY package*.json ./

# Zainstaluj zależności projektu
RUN npm install

# Skopiuj pozostałe pliki projektu
COPY . .

# Zbuduj aplikację (dla aplikacji TypeScript)
RUN npm run build

# Ustaw port, na którym nasłuchuje aplikacja
EXPOSE 3000

# Określ polecenie uruchamiające aplikację
CMD ["node", "dist/main"]
